// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем – повинні відображатися символи, які ввів користувач, 
// іконка змінює свій зовнішній вигляд. 
//  коментарях під іконкою – інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно – іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються – вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const passwordForm = document.body.querySelector('.password-form');
const btn = document.body.querySelector('.btn');

const inputPassword = document.querySelector('.input-password');
const confirmPassword = document.body.querySelector('.confirm-password');
const icon = document.getElementById('see');
const iconConfirm = document.getElementById('see2');

let noAuth = document.createElement('p')
    noAuth.innerText = 'Потрібно ввести однакові значення';
    noAuth.style.color = 'red';



function showPassword(input, elem) {
    passwordForm.addEventListener('click', (e) => {
        let event = e.target;
        e.preventDefault();

        console.log(event);
        if(event.id === elem.id){
            console.log(true);
            if(input.getAttribute('type') === 'password') {
                elem.className = "fas fa-eye-slash icon-password";
                input.removeAttribute('type');
                input.setAttribute('type', 'text')
            } else {
                elem.className = 'fas fa-eye icon-password';
                input.removeAttribute('type');
                input.setAttribute('type', 'password');
        }

            }
    });
    
}
    
showPassword(inputPassword, icon);
showPassword(confirmPassword, iconConfirm);



btn.addEventListener('mousedown', () => {
    if(inputPassword.value === confirmPassword.value) {
        alert('You are welcome!');
        inputPassword.value = null;
        confirmPassword.value = null;
    } else {
        document.body.append(noAuth);
    }
})


